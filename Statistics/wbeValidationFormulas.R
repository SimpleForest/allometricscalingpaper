#****************************************************************************
#
# Copyright (C) 2017-2020 Dr. Jan Hackenberg, free software developer
# All rights reserved.
#
# Contact : https://gitlab.com/simpleForest
#
# Developers : Dr. Jan Hackenberg
#
# This file is developed for SimpleForest plugin Version 1 for Computree.
#
# SimpleForest plugin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SimpleForest plugin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.
#
# SimpleForest is an extended version of the SimpleTree platform.
#
# *****************************************************************************/
# If you use this script, please cite:
# SimpleTree —An Efficient Open Source Tool to Build Tree Models from TLS Clouds
# I work on a more updated peer review paper and as soon I have a dedicated
# SimpleForest publication please rather cite the updated one. Not done yet though
# *****************************************************************************/
#install.packages('DescTools')
#install.packages('tidyverse')
#install.packages('data.table')
#install.packages('Metrics')
#install.packages('jcolors')
#install.packages('ggpmisc')
#install.packages('ggpubr')
#install.packages('jcolors')
#install.packages('tidyverse')
#install.packages('Metrics')

library('DescTools')
library('tidyverse')
library('data.table')
library('Metrics')
library('ggplot2')
library('ggpmisc')
library('ggpubr')
library('jcolors')
library('tidyverse')
library('data.table')
library('Metrics')
####################################################################
## Modify those paths ##############################################
####################################################################

PathToGT <- '~/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/Destructive_and_qsm_data_DEMOL.csv'
PathToQSMs <- '~/Documents/papers/data/PowerFilterVesselVolume/qsm/detailed/'

exportPath = '~/computree/bin/testScriptsAndData/DemolEtAl/test/'

######### 1 = Coarse Volume, 2 = Total Volume, 3 = Total Volume 5% range
processId = 2;

writeMode <- 2; # 1 = pdf, 2 = png

thresh <- 0.035;
if(processId > 1)
{
  thresh <- 0.0;
}
fac <- 1000000;
if(processId == 3)
{
  fac <- 1.05;
}

####################################################################
## Prepare the GT data #############################################
####################################################################

GTData <- read.csv(PathToGT)
index <- 1;
ori <- GTData$tree_name
tempName <- str_remove(GTData$tree_name, "-")
tempNameA <- substr(tempName,1,nchar(tempName)-1)
tempNameA <- str_remove(tempNameA, "0")
tempNameB <- substr(tempName,nchar(tempName),nchar(tempName))
tempName <- paste(tempNameA, tempNameB, sep = '')
ori <- GTData$tree_name
myNames <- data.frame(ori, tempName)
GTData$tree_name <- tempName
####################################################################
## Prepare the QSM data ############################################
####################################################################

fileList <- list.files(PathToQSMs)
croppedFileList <- substr(fileList,1,nchar(fileList)-4)


qsmAll <- data.frame()
index = 1
for(file in fileList)
{
  fullPath <- paste(PathToQSMs,file, sep = "")
  qsm <- read.csv(fullPath)
  GTTree <- GTData[which(GTData$tree_name == croppedFileList[index]),]
  qsm <- qsm[which(qsm$qualityFlag==1),]
  qsm <- qsm[which(qsm$FittingType==" SPHEREFOLLOWING"),]
  #qsm <- qsm[which(qsm$branchOrder>0),]
  qsm$radiusNorm <- qsm$radius/max(qsm$radius)
  qsm$vesselNorm <- qsm$vesselVolume/max(qsm$vesselVolume)
  qsm$growthVolumeNorm <- qsm$growthVolume/max(qsm$growthVolume)
  qsm$reversePipeAreaBranchorderNorm <- qsm$reversePipeAreaBranchorder/max(qsm$reversePipeAreaBranchorder)
  qsm$growthLengthNorm <- qsm$growthLength/max(qsm$growthLength)
  qsm$distanceToTwigNorm <- qsm$distanceToTwig/max(qsm$distanceToTwig)
  qsmAll <- rbind(qsmAll, qsm)
}


df <- data.frame(qsmAll$growthVolumeNorm, qsmAll$growthLengthNorm, qsmAll$reversePipeAreaBranchorderNorm, qsmAll$vesselNorm, qsmAll$radiusNorm, qsmAll$distanceToTwigNorm)
#df <- data.frame(qsmAll$growthVolume, qsmAll$growthLength, qsmAll$reversePipeAreaBranchorder, qsmAll$vesselVolume, qsmAll$radius, qsmAll$distanceToTwig)
names(df) <- c("GrowthVolume", "GrowthLength", "RBOPA", "VesselVolume", "Radius", "distanceToTwig" )
df2 <- log(df)
names(df2) <- c("ln(GrowthVolume)", "ln(GrowthLength)", "ln(RBOPA)", "ln(VesselVolume)", "ln(Radius)", "ln(distanceToTwig)" )
y <- df2$`ln(Radius)`
x <- df2$`ln(GrowthVolume)`

title <- "Performing the WBE test"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthVolume)") + 
  ylab("ln(Radius)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthVolumeRadius.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)


x <- df2$`ln(GrowthVolume)`
y <- df2$`ln(RBOPA)`

title <- "Performing the WBE test"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthVolume)") + 
  ylab("ln(RBOPA)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthVolumeRBOPA.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)



x <- df2$`ln(GrowthVolume)`
y <- df2$`ln(distanceToTwig)`

title <- "Performing the WBE test"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthVolume)") + 
  ylab("ln(distanceToTip)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthVolumeDistanceToTwig.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)

model <- lm(y~x)
model$coefficients
model$coefficients[2]
3/4




x <- df2$`ln(Radius)`
y <- df2$`ln(RBOPA)`

title <- "Performing the WBE test"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(RBOPA)") + 
  ylab("ln(Radius)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnRBOPARadius.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)


x <- df2$`ln(GrowthVolume)`
y <- df2$`ln(GrowthLength)`

title <- "Giving new WBE relevant input"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthVolume)") + 
  ylab("ln(GrowthLength)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthVolumeGrowthLength.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)

x <- df2$`ln(VesselVolume)`
y <- df2$`ln(Radius)`

title <- "Giving new WBE relevant input"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) +  
  geom_point() + 
  `geom_smooth`(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +  
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(VesselVolume)") + 
  ylab("ln(Radius)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnVesselVolumeRadius.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)

x <- df2$`ln(GrowthVolume)`
y <- df2$`ln(VesselVolume)`

title <- "Giving new WBE relevant input"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthVolume)") + 
  ylab("ln(VesselVolume)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthVolumeVesselVolume.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)

x <- df2$`ln(GrowthLength)`
y <- df2$`ln(RBOPA)`
title <- "Giving new WBE relevant input"
my.formula <- y ~ x
p <- ggplot(df2, aes(x, y)) + 
  geom_point() + 
  geom_smooth(method = "lm", se=FALSE, formula = my.formula) +
  stat_poly_eq(label.y = "bottom", label.x = "right", formula = my.formula, 
               aes(label = paste(..eq.label.., ..adj.rr.label.., sep = "~~~")), 
               parse = TRUE) +   
  ggtitle(title, "Demol et al data - each tree is normalized then log transformed") +
  theme_classic() + 
  xlab("ln(GrowthLength)") + 
  ylab("ln(RBOPA)") +
  theme(aspect.ratio=1)
ggpar(p, palette = "jco")
print(p, palette = "jco")
pdf_file <- '/home/drsnuggles/Documents/papers/SimpleForest/allometricscalingpaper/Statistics/WBElnGrowthLengthRBOPA.pdf'
ggsave(pdf_file)
knitr::plot_crop(pdf_file)
